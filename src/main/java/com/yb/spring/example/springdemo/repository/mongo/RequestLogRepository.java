package com.yb.spring.example.springdemo.repository.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface RequestLogRepository extends MongoRepository<RequestLog, String> {
}