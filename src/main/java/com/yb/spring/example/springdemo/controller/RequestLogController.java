package com.yb.spring.example.springdemo.controller;

import com.yb.spring.example.springdemo.repository.mongo.RequestLog;
import com.yb.spring.example.springdemo.service.RequestLogService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("")
public class RequestLogController {

    private final RequestLogService requestLogService;

    public RequestLogController(@Qualifier("requestLogService") final RequestLogService requestLogService) {
        this.requestLogService = requestLogService;
    }

    @GetMapping("/logs")
    @ResponseBody
    public List<RequestLog> findAll() {
        return requestLogService.allLogs();
    }

    @GetMapping("/logs/user/{userId}")
    @ResponseBody
    public List<RequestLog> findByUser(@PathVariable("userId") final String userId) {
        return requestLogService.logsByUser(userId);
    }

    @GetMapping("/logs/{id}")
    @ResponseBody
    public RequestLog findById(@PathVariable("id") final String logId) {
        return requestLogService.logsById(logId);
    }

    @DeleteMapping("/logs/{id}")
    @ResponseBody
    public RequestLog removeById(@PathVariable("id") final String logId) {
        return requestLogService.logsById(logId);
    }
}
