package com.yb.spring.example.springdemo.validation;

import java.util.Set;

public class BookUnSupportedFieldPatchException extends RuntimeException {

    public BookUnSupportedFieldPatchException(final Set<String> keys) {
        super("Field " + keys.toString() + " update is not allow.");
    }

}