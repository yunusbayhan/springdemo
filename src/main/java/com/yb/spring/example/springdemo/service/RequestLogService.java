package com.yb.spring.example.springdemo.service;

import com.yb.spring.example.springdemo.repository.mongo.RequestLog;

import java.util.List;

public interface RequestLogService {
    void saveLog(RequestLog requestLog);

    List<RequestLog> allLogs();

    RequestLog logsById(String userId);

    List<RequestLog> logsByUser(String logId);

    void deleteById(String logId);
}
