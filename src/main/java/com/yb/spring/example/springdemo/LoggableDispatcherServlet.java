package com.yb.spring.example.springdemo;

import com.yb.spring.example.springdemo.repository.mongo.RequestLog;
import com.yb.spring.example.springdemo.service.RequestLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;
import org.springframework.web.util.WebUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

@Slf4j
public class LoggableDispatcherServlet extends DispatcherServlet {

    @Resource(name = "requestLogService")
    private RequestLogService requestLogService;

    @Override
    protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (!(request instanceof ContentCachingRequestWrapper)) {
            request = new ContentCachingRequestWrapper(request);
        }
        if (!(response instanceof ContentCachingResponseWrapper)) {
            response = new ContentCachingResponseWrapper(response);
        }
        final HandlerExecutionChain handler = getHandler(request);

        try {
            super.doDispatch(request, response);
        } finally {
            log(request, response, handler);
            updateResponse(response);
        }
    }

    private void log(final HttpServletRequest requestToCache, final HttpServletResponse responseToCache, final HandlerExecutionChain handler) {

        final RequestLog requestLog = RequestLog.builder() //
                .requestTime(Long.valueOf(System.nanoTime())) //
                .requestMethod(requestToCache.getMethod())//
                .userName(requestToCache.getRemoteUser())//
                .response(getResponsePayload(responseToCache)) //
                .requestPath(requestToCache.getRequestURI())//
                .build();

        requestLogService.saveLog(requestLog);
    }

    private String getResponsePayload(final HttpServletResponse response) {
        final ContentCachingResponseWrapper wrapper = WebUtils.getNativeResponse(response, ContentCachingResponseWrapper.class);
        if (wrapper != null) {

            final byte[] buf = wrapper.getContentAsByteArray();
            if (buf.length > 0) {
                final int length = Math.min(buf.length, 5120);
                try {
                    return new String(buf, 0, length, wrapper.getCharacterEncoding());
                } catch (final UnsupportedEncodingException ex) {
                    // NOOP
                }
            }
        }
        return "[unknown]";
    }

    private void updateResponse(final HttpServletResponse response) throws IOException {
        final ContentCachingResponseWrapper responseWrapper =
                WebUtils.getNativeResponse(response, ContentCachingResponseWrapper.class);
        responseWrapper.copyBodyToResponse();
    }

}