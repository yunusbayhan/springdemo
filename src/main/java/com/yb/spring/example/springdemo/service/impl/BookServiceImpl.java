package com.yb.spring.example.springdemo.service.impl;

import com.yb.spring.example.springdemo.repository.rdms.Book;
import com.yb.spring.example.springdemo.repository.rdms.BookRepository;
import com.yb.spring.example.springdemo.service.BookService;
import com.yb.spring.example.springdemo.validation.ItemNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service("bookService")
public class BookServiceImpl implements BookService {

    public static final String BOOK_LIST_CACHE_KEY = "allBooks";
    private final BookRepository repository;

    public BookServiceImpl(@Autowired final BookRepository repository) {
        this.repository = repository;
    }

    @Cacheable(cacheNames = "bookList", key = "#root.target.BOOK_LIST_CACHE_KEY")
    @Override
    public List<Book> findAll() {
        log.info("not cached");
        return repository.findAll();
    }

    @Cacheable(cacheNames = "book", key = "#id")
    @Override
    public Book findOne(final Long id) {
        log.info("not cached");
        return repository.findById(id)
                .orElseThrow(() -> new ItemNotFoundException(String.valueOf(id)));
    }

    @CacheEvict(value = "book", allEntries = true)
    @Override
    public Book newBook(final Book newBook) {
        return repository.save(newBook);
    }

    @Caching(evict = @CacheEvict(value = "bookList", allEntries = true), put = @CachePut(value = "book", key = "#id"))
    @Override
    public Book saveOrUpdate(final Book newBook, final Long id) {

        return repository.findById(id)
                .map(x -> {
                    x.setName(newBook.getName());
                    x.setAuthor(newBook.getAuthor());
                    x.setPrice(newBook.getPrice());
                    return repository.save(x);
                })
                .orElseGet(() -> {
                    newBook.setId(id);
                    return repository.save(newBook);
                });
    }

    @Caching(evict = {
            @CacheEvict(value = "bookList", allEntries = true),
            @CacheEvict(value = "book", key = "#id")
    })
    @Override
    public void deleteBook(final Long id) {
        repository.deleteById(id);
    }

}
