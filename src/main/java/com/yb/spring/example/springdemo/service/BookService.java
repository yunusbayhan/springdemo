package com.yb.spring.example.springdemo.service;

import com.yb.spring.example.springdemo.repository.rdms.Book;

import java.util.List;

public interface BookService {

    List<Book> findAll();

    Book newBook(Book newBook);

    Book findOne(Long id);

    Book saveOrUpdate(Book newBook, Long id);

    void deleteBook(Long id);
}
