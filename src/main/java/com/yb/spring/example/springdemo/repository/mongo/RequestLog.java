package com.yb.spring.example.springdemo.repository.mongo;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

@ToString
@Setter
@Getter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class RequestLog {

    @Id
    public ObjectId _id;

    String requestMethod;
    String requestPath;
    String response;
    Long requestTime;
    String userName;

    public String get_id() {
        return _id.toHexString();
    }
}
