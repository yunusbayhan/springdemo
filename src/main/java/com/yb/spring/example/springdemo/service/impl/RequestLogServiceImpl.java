package com.yb.spring.example.springdemo.service.impl;

import com.yb.spring.example.springdemo.repository.mongo.RequestLog;
import com.yb.spring.example.springdemo.repository.mongo.RequestLogRepository;
import com.yb.spring.example.springdemo.service.RequestLogService;
import com.yb.spring.example.springdemo.validation.ItemNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("requestLogService")
public class RequestLogServiceImpl implements RequestLogService {

    private final RequestLogRepository requestLogRepository;

    public RequestLogServiceImpl(@Autowired final RequestLogRepository requestLogRepository) {
        this.requestLogRepository = requestLogRepository;
    }

    @Override
    public void saveLog(final RequestLog requestLog) {
        requestLogRepository.save(requestLog);
    }

    @Override
    public List<RequestLog> allLogs() {
        return requestLogRepository.findAll();
    }

    @Override
    public RequestLog logsById(final String id) {
        return requestLogRepository.findById(id).orElseThrow(() -> new ItemNotFoundException(id));
    }

    @Override
    public List<RequestLog> logsByUser(final String userId) {

        final Example<RequestLog> example = Example.of(RequestLog.builder().userName(userId).build());
        return requestLogRepository.findAll(example);
    }

    @Override
    public void deleteById(final String logId) {
        requestLogRepository.deleteById(logId);
    }
}
