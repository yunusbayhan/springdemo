package com.yb.spring.example.springdemo.validation;

public class ItemNotFoundException extends RuntimeException {

    public ItemNotFoundException(final String id) {
        super("Item not found id : " + id);
    }

}