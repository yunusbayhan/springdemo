package com.yb.spring.example.springdemo.controller;

import com.yb.spring.example.springdemo.repository.rdms.Book;
import com.yb.spring.example.springdemo.service.BookService;
import com.yb.spring.example.springdemo.validation.ItemNotFoundException;
import com.yb.spring.example.springdemo.validation.BookUnSupportedFieldPatchException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@Validated
public class BookController {

    private final BookService bookService;

    public BookController(@Qualifier("bookService") final BookService bookService
    ) {
        this.bookService = bookService;
    }

    // Find
    @GetMapping("/books")
    public List<Book> findAll() {
        return bookService.findAll();
    }

    // Save
    @PostMapping("/books")
    @ResponseStatus(HttpStatus.CREATED)
    public Book newBook(@Valid @RequestBody final Book newBook) {
        return bookService.newBook(newBook);
    }

    // Find
    @GetMapping("/books/{id}")
    public Book findOne(@PathVariable @Min(1) final Long id) {
        return bookService.findOne(id);
    }

    // Save or update
    @PutMapping("/books/{id}")
    public Book saveOrUpdate(@Valid @RequestBody final Book newBook, @PathVariable final Long id) {

        return bookService.saveOrUpdate(newBook, id);
    }

    // update author only
    @PatchMapping("/books/{id}")
    public Book patch(@RequestBody final Map<String, String> update, @PathVariable final Long id) {

        return Optional.ofNullable(bookService.findOne(id))
                .map(x -> {

                    String author = update.get("author");
                    if (!StringUtils.isEmpty(author)) {
                        x.setAuthor(author);

                        // better create a custom method to update a value = :newValue where id = :id
                        return bookService.newBook(x);
                    } else {
                        throw new BookUnSupportedFieldPatchException(update.keySet());
                    }

                })
                .orElseGet(() -> {
                    throw new ItemNotFoundException(String.valueOf(id));
                });
    }

    @DeleteMapping("/books/{id}")
    public void deleteBook(@PathVariable final Long id) {
        bookService.deleteBook(id);
    }
}
