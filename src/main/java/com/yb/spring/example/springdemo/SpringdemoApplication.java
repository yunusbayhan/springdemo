package com.yb.spring.example.springdemo;

import com.yb.spring.example.springdemo.repository.rdms.Book;
import com.yb.spring.example.springdemo.repository.rdms.BookRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;

@EnableCaching
@SpringBootApplication
//@EnableConfigurationProperties TODO use the annotation
public class SpringdemoApplication {

    public static void main(final String[] args) {
        SpringApplication.run(SpringdemoApplication.class, args);
    }

    @Bean
    CommandLineRunner initDatabase(final BookRepository repository) {
        return args -> {
            repository.save(new Book("A Guide to the Bodhisattva Way of Life", "Santideva", new BigDecimal("15.41")));
            repository.save(new Book("The Life-Changing Magic of Tidying Up", "Marie Kondo", new BigDecimal("9.69")));
            repository.save(new Book("Refactoring: Improving the Design of Existing Code", "Martin Fowler", new BigDecimal("47.99")));
        };
    }
}
