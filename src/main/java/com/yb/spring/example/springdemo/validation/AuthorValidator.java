package com.yb.spring.example.springdemo.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class AuthorValidator implements ConstraintValidator<Author, String> {

    List<String> authors = Arrays.asList("Santideva", "Marie Kondo", "Martin Fowler", "bayhanus");

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {

        return authors.contains(value);

    }
}